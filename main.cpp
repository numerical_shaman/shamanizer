
// std
#include <iostream>
// clang
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/ArgumentsAdjusters.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Rewrite/Core/Rewriter.h"
// shamanizer
#include "src/parameters.h"
#include "src/String.h"
#include "src/CmdArguments.h"
#include "src/MatcherAction.h"

// Apply a custom category to all command-line options so that they are the only ones displayed.
static llvm::cl::OptionCategory ShamanizerOptions("Shamanizer options");

// CommonOptionsParser declares HelpMessage with a description of the common command-line options related to the compilation database and input files.
static llvm::cl::extrahelp CommonHelp(clang::tooling::CommonOptionsParser::HelpMessage);

//-----------------------------------------------------------------------------
// MAIN

#include "src/String.h"
#include "src/CmdArguments.h"
#include "src/MatcherAction.h"

// tool build using libtooling
// inspired by : clangRename
int main(int argc, const char **argv)
{
    // get compilation database and parses inputs
    tooling::CommonOptionsParser OptionsParser(argc, argv, ShamanizerOptions);

    // get files
    auto files = OptionsParser.getSourcePathList();
    if((files.size() == 1) && (files[0] == "."))
    {
        // get all files in compilation database when '.' is used as single argument (instead of a file list)
        files = OptionsParser.getCompilations().getAllFiles();
    }

    // build a refactoring tool
    tooling::RefactoringTool Tool(OptionsParser.getCompilations(), files);

    auto clangIncludesArg = clang::tooling::getInsertArgumentAdjuster(clangIncludes,
        clang::tooling::ArgumentInsertPosition::BEGIN);
    Tool.appendArgumentsAdjuster(clangIncludesArg);

    // extract paths to included folders (lines starting with -I)
    std::vector<std::string> includeDirectories = extractIncludeFolders(OptionsParser.getCompilations());
    //includeDirectories.push_back(clangIncludePath);
    //includeDirectories.push_back(cppIncludePath);

    // builds an action that will search for our targets
    ReplacementsMap replacementsMap(&Tool.getReplacements(), includeDirectories);
    MyFrontendAction MatchingAction = MyFrontendAction(replacementsMap);

    // runs the action on the AST
    int ExitCode;
    if (InPlace)
    {
        // run tool then refactor (will not refactor in case of error)
        ExitCode = Tool.runAndSave(tooling::newFrontendActionFactory(&MatchingAction).get());
    }
    else
    {
        // run tool
        ExitCode = Tool.run(tooling::newFrontendActionFactory(&MatchingAction).get());

        // Write every file to stdout.
        LangOptions DefaultLangOptions;
        IntrusiveRefCntPtr<DiagnosticOptions> DiagOpts = new DiagnosticOptions();
        TextDiagnosticPrinter DiagnosticPrinter(llvm::errs(), &*DiagOpts);
        DiagnosticsEngine Diagnostics(IntrusiveRefCntPtr<DiagnosticIDs>(new DiagnosticIDs()), &*DiagOpts, &DiagnosticPrinter, false);
        auto& FileMgr = Tool.getFiles();
        SourceManager Sources(Diagnostics, FileMgr);
        Rewriter Rewrite(Sources, DefaultLangOptions);
        Tool.applyAllReplacements(Rewrite);
        //for (const auto &file : files) // iter on all files from the database
        for(auto kv : Tool.getReplacements()) // iter on all files that have been modified
        {
            const auto& file = kv.first;
            const auto *Entry = FileMgr.getFile(file);
            const auto ID = Sources.getOrCreateFileID(Entry, SrcMgr::C_User);
            llvm::outs() << "\n\nFILE : " << file << '\n';
            Rewrite.getEditBuffer(ID).write(llvm::outs());
        }
    }

    return ExitCode;
}
