# Links and objectives for the developement of Shamanizer

## TODO :

List of improvements that could be incorporated into the tool :

- some files, mostly headers, appear to be instrumented several times (constants wrapped several times and function block added several times)
- an option to not include tagged error (function_block) would be good when the user wants to do it himself
- constants such as `M_PI`, `M_LN2` and `M_PI_2` should be wrapped automatically but are untouched when dealing with complex numbers
- helpers whould be included *after* Shaman to avoid include problems
- I have seen the Eigen arraybase pow function mistakenly turned into Sstd::pow producing a meaningless one argument pow

- some warnings (such as warnings on free) are not needed anymore
- put #include in a better place (before first non whiteline/comment line)
- put function tag in a better place (on first line of function)
- use integers (deduced from callgraph) for function tags
- add MPI support
- wraps numbers (float/integers) in operators with eigen

## Useful links :

### Tutorials :

- [Tutorial for building tools using LibTooling and LibASTMatchers](http://clang.llvm.org/docs/LibASTMatchersTutorial.html)
  - [Building a tool with libTooling](http://clang.llvm.org/docs/LibTooling.html)
  - [LLVM](https://github.com/llvm-mirror/llvm)
  - [Clang](https://github.com/llvm-mirror/clang)
  - [Clang-tools-extra](https://github.com/llvm-mirror/clang-tools-extra)
  - [Embedding LLVM in your project](http://llvm.org/docs/CMake.html#embedding-llvm-in-your-project)
- [Getting Started with Clang Refactoring Tools](https://variousburglarious.com/2017/01/18/getting-started-with-clang-refactoring-tools/)
  - [Refactoring tools and replacements](https://eli.thegreenplace.net/2014/07/29/ast-matchers-and-clang-refactoring-tools)
  - [Using Replacements to Transform Source Code](https://variousburglarious.com/2017/03/21/using-replacements-to-transform-source-code/)
  - [Clang's refactoring engine](http://clang.llvm.org/docs/RefactoringEngine.html)
- [Modern source-to-source transformation with Clang and libTooling](https://eli.thegreenplace.net/2014/05/01/modern-source-to-source-transformation-with-clang-and-libtooling)

### Various :

- [Node Matcher](http://clang.llvm.org/docs/LibASTMatchersReference.html#decl-matchers)
- [How to parse only user defined source files with clang tools](https://stackoverflow.com/questions/50040832/how-to-parse-only-user-defined-source-files-with-clang-tools)

http://clang.llvm.org/doxygen/classclang_1_1tooling_1_1Replacement.html
http://clang.llvm.org/doxygen/classclang_1_1tooling_1_1Replacements.html
https://stackoverflow.com/questions/25129880/rewrite-codes-which-matches-a-certain-astmatcher
https://github.com/eliben/llvm-clang-samples/blob/master/src_clang/matchers_rewriter.cpp
https://kevinaboos.wordpress.com/2013/07/30/clang-tips-and-tricks/
https://stackoverflow.com/questions/27029313/whats-the-right-way-to-match-includes-or-defines-using-clangs-libtooling
https://stackoverflow.com/questions/44027316/clang-libtooling-insert-a-new-header-safely
https://xaizek.github.io/2015-04-23/detecting-wrong-first-include/
https://stackoverflow.com/tags/libtooling/hot?filter=all
https://stackoverflow.com/questions/27029313/whats-the-right-way-to-match-includes-or-defines-using-clangs-libtooling/27075471#27075471
https://stackoverflow.com/questions/43448448/how-to-get-source-location-of-includes-using-clang-libtooling/44077744#44077744

