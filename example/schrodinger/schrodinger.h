#ifndef COMPENSATIONS_SCHRODINGER_H
#define COMPENSATIONS_SCHRODINGER_H

#include <vector>

/*
 * Solving the Schrödinger Equation using the Numerov algorithm
 */
class Schrodinger
{
private:
    // constants
    std::vector<double> psi;
    double ECurrent;
    double EMin = 1.490;
    double xMin = -15.;
    double xMax = 15.;
    double hZero;
    double EDelta = 1e-7;
    double maxPsi = 1e-8;
    int numberDivisions = 200;

    // computations
    double calculateKSSquared(int n);
    double calculateNextPsi(int n);

public:
    // initialisation
    Schrodinger();

    // computations
    void calculate();
};

#endif //COMPENSATIONS_SCHRODINGER_H