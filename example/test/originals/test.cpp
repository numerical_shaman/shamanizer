
#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <cstdio>

#include <distant_library.h>

#define MacroNumber double
#define MacroRef double&
using UsingNumber = double;
typedef double DefNumber;

#define X 5.5
#define ADD 1. + 6.
#define DECL double de = 0.
#define COSINUS std::cos(4.)

// this function should not be tagged
double integer_test(double x)
{
    int result = 0;

    for(int i = 0; i < 10; i++)
    {
        result += 5 - std::max(result, i);
    }

    return x;
}

// test function namespace
double function_test(double x)
{
    double y = cos(x);
    double z = std::cos(5.0);
    double w = cos(1);
    double g = COSINUS;
    return 0;
}

// test type declaration and manipulation
double print_test(double x)
{
    double y = 1. / (2. + 6.); // literals in operations should be wrapped
    double z = x + y;
    double* p = &z;
    double& rd = y;
    double additionres = ADD;
    DECL;

    MacroNumber m = 0.f;
    MacroRef rm = m;
    UsingNumber u = 0.L;
    DefNumber d = 0. + X;

    printf("double : %.6f %c %s",y,'n',"a text example");
    printf("double ref : %s %.6f %c","a text example with a %f",rd,'n');
    printf("double macro : %f",m);
    printf("double using : %f",u);
    printf("double typedef : %f",d);

    // \ in string should be kept
    printf("%f \n \t \" %f",y,y);

    // should wrap the full method and not just vec
    std::vector<double> vec(10,0.);
    printf("%f", vec.front());

    return z;
}

// T should not be replaced by a Sdouble
template <typename T> T* Allocate(size_t size) { return new T[size]; }
// T should not be replaced by a Sdouble
template <typename T> void Release(T** ptr) { delete[] (*ptr); }
// float should be replaced by Sfloat
template <> void Release<float>(float** ptr) { delete[] (*ptr); }

void memory_test()
{
    // basic malloc
    int n = 100;
    double* tabM = (double*)malloc(n * sizeof(double));
    free(tabM);

    // new[] alloc
    double* tabN = new double[n];
    delete[] tabN;

    // indirect alloc
    double* tabA = Allocate<double>(n);
    Release(&tabM);
}

int main()
{
    std::vector<double> vec;
    double res = print_test(3);
    std::cout << res << std::endl;

    double distantRes = distant_library::lib_function(res);

    return 0;
}