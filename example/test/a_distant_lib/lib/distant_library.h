#ifndef SHAMANIZER_DISTANT_LIBRARY_H
#define SHAMANIZER_DISTANT_LIBRARY_H

// a distant library that should not be instrumented (being out of the main code)
namespace distant_library
{
    double lib_function(double x)
    {
        double y = x + 1;
        return y;
    }
}

#endif //SHAMANIZER_DISTANT_LIBRARY_H