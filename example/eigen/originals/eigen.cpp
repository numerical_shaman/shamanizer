#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using my_eigen_type = Vector3d;
#define MY_EIGEN_TYPE VectorXd

int main()
{
    // should get only the usual instrumentation
    double x = 5 + (6. + 5.);

    // run time size
    {
        Eigen::MatrixXd m = MatrixXd::Random(3,3);
        m = (m + MatrixXd::Constant(3,3,1.2 + 0.6)) * 50;
        std::cout << "m =" << std::endl << m << std::endl;
        MY_EIGEN_TYPE v(3);
        v << 1, 2, 3;
        std::cout << "m * v =" << std::endl << m * v << std::endl;
    }

    // compile time size
    {
        Matrix3d m = Matrix3d::Random();
        m = (m + Matrix3d::Constant(1.2)) * (25 + 50);
        std::cout << "m =" << std::endl << m << std::endl;
        my_eigen_type v(1,2,3);
        std::cout << "m * v =" << std::endl << m * v << std::endl;

        Matrix3d& m2 = m;
        Matrix3d* m3 = &m2;
    }
}
