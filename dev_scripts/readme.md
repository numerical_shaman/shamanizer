# Developement scripts

This folder contains various shell scripts used to help in the developement of Shamanizer.
In particular :

- `dump.sh` which is used to dump the AST of a file (to look for new patterns to add to Shamanizer)
- `query.sh` to test a pattern match on a file
- `test.sh` which is used to apply Shamanizer to a given source code

