cmake_minimum_required(VERSION 3.9)
project(shamanizer)

# compiler
set(LLVM_PREFIX_PATH /ccc/temp/cont001/ocre/demeuren/clang-llvm/build)
#set(LLVM_PREFIX_PATH /ccc/temp/cont001/ocre/chevalierc/spack_install/spack/opt/spack/linux-centos7-x86_64/gcc-4.8.5/llvm-6.0.0-ecwcr7khey6y2zbw27tgk5q3cike4s2q)
set(CMAKE_CXX_STANDARD 17)
#set(CMAKE_CXX_COMPILER "${LLVM_PREFIX_PATH}/bin/clang++") # error no member named 'is_final' in namespace 'std'
set(CMAKE_CXX_COMPILER g++)

# llvm
#set(LLVM_DIR "${LLVM_PREFIX_PATH}/lib/cmake/llvm")
find_package(LLVM REQUIRED CONFIG)
include_directories(${LLVM_INCLUDE_DIRS})
add_definitions(${LLVM_DEFINITIONS})
# LLVM libs used
llvm_map_components_to_libnames(llvm_libs support core)

# clang
#set(Clang_DIR "${LLVM_PREFIX_PATH}/lib/cmake/clang")
find_package(Clang REQUIRED CONFIG)
include_directories(${CLANG_INCLUDE_DIRS})
add_definitions(${CLANG_DEFINITIONS})
# clang libs used
set(clang_libs clangTooling clangBasic clangASTMatchers)

# flags
add_definitions("-fno-rtti") # solves typeinfo problem (should be useless if llvm is compiled with this, non default, flag)

# files
set(SOURCE_FILES
        src/matchCallbacks/eigen/changeEigenTypes.h
        src/matchCallbacks/eigen/eigenLiteralWrapping.h
        src/matchCallbacks/changeTypes.h
        src/matchCallbacks/floatLiteralWrapping.h
        src/matchCallbacks/mathFunctions.h
        src/matchCallbacks/functionTagger.h
        src/matchCallbacks/printRewriting.h
        src/matchCallbacks/mallocWarning.h
        src/ClangUtilities.h
        src/MatcherAction.h
        src/CmdArguments.h
        src/String.h
        src/MyMatchers.h
        src/parameters.h)

set(SHAMANIZER_CLANG_STD_INCLUDE ${LLVM_LIBRARY_DIR}/clang/${LLVM_PACKAGE_VERSION}/include)
set(SHAMANIZER_CLANG_CPP_INCLUDE ${LLVM_INCLUDE_DIRS}/c++/v1)
CONFIGURE_FILE(shamanizer_config.h.in shamanizer_config.h @ONLY)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

# linking
add_executable(shamanizer main.cpp ${SOURCE_FILES})
target_link_libraries(shamanizer ${llvm_libs} ${clang_libs})

enable_testing()
add_subdirectory(example/test)

install(TARGETS shamanizer
        RUNTIME DESTINATION bin)

