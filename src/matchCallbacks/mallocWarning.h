#ifndef SHAMANIZER_MALLOCWARNING_H
#define SHAMANIZER_MALLOCWARNING_H

// clang
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
// shamanizer
#include "../ClangUtilities.h"
#include "../MyMatchers.h"

//---------------------------------------------------------------------------------------
// MATCHERS

/*
 * matches names of unsafe memory manipulation functions
 */
auto MemoryFunctionNameMatcher =
        hasAnyName("malloc","calloc","realloc","free");

/*
 * matches unsafe memory manipulation functions
 */
auto MemoryFunctionMatcher = [](const std::vector<std::string>& includeFolders)
{
    return callExpr(my_matchers::isInProject(includeFolders),
                    callee(functionDecl(MemoryFunctionNameMatcher).bind("function"))
    ).bind("callexpr");
};

//---------------------------------------------------------------------------------------
// FUNCTIONS

/*
 * displays a warning message about the malloc and provide a guide to translating then to modern C++ if need be
 */
void warningMessage(const std::string& functionName, const std::string& location)
{
    llvm::errs() << '\n' << "WARNING : "
              << "We detected a call to " << functionName << " at " << location << '\n'
              << "It might lead to incorrect results or segfaults if used to manipulate memory reserved for Shaman's types." << '\n'
              << "As a safe alternative you might consider using :" << '\n'
              << " - `T* a = new T` instead of `T* a = (T*)malloc(sizeof(T))`" << '\n'
              << " - `T* b = new T[N]` instead of `T* b = (T*)malloc(N * sizeof(T))`" << '\n'
              << " - `delete a` instead of `free(a)`" << '\n'
              << " - `delete[] b` instead of `free(b)`" << '\n'
              << '\n';
}

//---------------------------------------------------------------------------------------
// HANDLER

/*
 * raises a warning when a malloc/free is found
 * (the task is too hard and not common enough to translate mallocs into new automatically)
 */
class MallocWarner : public MatchFinder::MatchCallback
{
private:
    ReplacementsMap& fileToReplacements;

public :
    // constructor
    MallocWarner(ReplacementsMap& fileToReplacements): fileToReplacements(fileToReplacements) {}

    // action that runs on matches
    virtual void run(const MatchFinder::MatchResult &Result)
    {
        // get the node
        auto node = Result.Nodes.getNodeAs<CallExpr>("callexpr");
        auto sourceMgr = Result.SourceManager;
        SourceLocation location = Localisation::getMacroLoc(sourceMgr, node->getLocStart());
        std::string filename = sourceMgr->getFilename(location);

        // get the function name
        auto functionNode = Result.Nodes.getNodeAs<FunctionDecl>("function"); // the function declaration
        std::string functionName = functionNode->getNameAsString();

        // outputs a warning message
        std::string locationString = location.printToString(*sourceMgr);
        warningMessage(functionName, locationString);
    }
};

#endif //SHAMANIZER_MALLOCWARNING_H