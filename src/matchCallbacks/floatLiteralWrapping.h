#ifndef SHAMANIZER_FLOATLITERALWRAPPING_H
#define SHAMANIZER_FLOATLITERALWRAPPING_H

// clang
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
// shamanizer
#include "../ClangUtilities.h"
#include "../MyMatchers.h"
#include "changeTypes.h"
#include "mathFunctions.h"

//---------------------------------------------------------------------------------------
// MATCHERS

/*
 * match double literal
 * that are in the project files
 * and are used by a binary operator or a math function (other literals will be converted implicitly without problems)
 */
auto DoubleLiteralMatcher = [](const std::vector<std::string>& includeFolders)
{
    return floatLiteral(my_matchers::isInProject(includeFolders),
                  anyOf(hasParent(binaryOperator()),
                        hasParent(MathematicFunctionMatcher(includeFolders)))
    ).bind("literal");
};

//---------------------------------------------------------------------------------------
// HANDLER

/*
 * wraps double literal in Sdouble
 */
class LiteralWrapper : public MatchFinder::MatchCallback
{
private:
    ReplacementsMap& fileToReplacements;

public :
    // constructor
    LiteralWrapper(ReplacementsMap& R) : fileToReplacements(R) {}

    // action that runs on matches
    virtual void run(const MatchFinder::MatchResult &Result)
    {
        // get the node
        auto node = Result.Nodes.getNodeAs<FloatingLiteral>("literal");
        auto sourceMgr = Result.SourceManager;
        auto location = Localisation::getMacroLoc(sourceMgr, node->getLocStart());

        // wraps the litteral : 5.5 -> Sdouble(5.5)
        std::string type = node->getType().getAsString();
        Replace::wrapNode(fileToReplacements, sourceMgr, node, StypeOfType(type) + "(", ")");
        Headers::addHeader(fileToReplacements, sourceMgr, location, Headers::shamanHeader);

        // localisation of the match in human readable format
        std::string readableLoc = location.printToString(*sourceMgr);
        DebugLog("Matched " + type + " literal at : " + readableLoc);
    }
};

#endif //SHAMANIZER_FLOATLITERALWRAPPING_H