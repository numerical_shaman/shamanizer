#ifndef SHAMANIZER_FUNCTIONTAGGER_H
#define SHAMANIZER_FUNCTIONTAGGER_H

// clang
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
// shamanizer
#include "../ClangUtilities.h"
#include "../MyMatchers.h"
#include "mathFunctions.h"

//---------------------------------------------------------------------------------------
// MATCHERS

/*
 * matches an arithmetic operation
 * unless it operates on integers
 */
auto ArithmeticOperatorMatcher =
        binaryOperator(unless(hasEitherOperand(hasType(isInteger()))),
                       anyOf(hasOperatorName("+"), hasOperatorName("-"), hasOperatorName("/"), hasOperatorName("*"),
                             hasOperatorName("+="), hasOperatorName("-="), hasOperatorName("/="), hasOperatorName("*=")));

/*
 * matches incr operators
 * unless the argument is an interger
 */
auto IncrOperatorMatcher =
        unaryOperator(unless(hasUnaryOperand(hasType(isInteger()))),
                      anyOf(hasOperatorName("++"), hasOperatorName("--")));

/*
 * matches code that contains operations that could generate error :
 * arithmetic operations or mathematical functions
 */
auto ErrorProneOperation = [](const std::vector<std::string>& includeFolders)
{
    return anyOf(hasDescendant(ArithmeticOperatorMatcher),
                 hasDescendant(IncrOperatorMatcher),
                 hasDescendant(MathematicFunctionMatcher(includeFolders)));
};

/*
 * match function bodies
 */
auto FunctionBodyMatcher = [](const std::vector<std::string>& includeFolders)
{
    return functionDecl(my_matchers::isInProject(includeFolders),
                        hasBody(compoundStmt(ErrorProneOperation(includeFolders)).bind("functionBody"))
    ).bind("function");
};

//---------------------------------------------------------------------------------------
// HANDLER

/*
 * adds tags to the beginning of functions containing either arithmetic operators or mathematical functions
 * TODO improve position of tag
 * TODO precompute tag index at compile time
 */
class FunctionBodyTagger : public MatchFinder::MatchCallback
{
private:
    ReplacementsMap& fileToReplacements;

public :
    // constructor
    FunctionBodyTagger(ReplacementsMap& R) : fileToReplacements(R) {}

    // action that runs on matches
    virtual void run(const MatchFinder::MatchResult &Result)
    {
        // get the node
        auto node = Result.Nodes.getNodeAs<Stmt>("functionBody");
        auto sourceMgr = Result.SourceManager;

        // adds a tag to the beginning of the function body {} -> {FUNCTION_BLOCK;}
        auto location = Localisation::getLocAfterToken(sourceMgr, Localisation::getMacroLoc(sourceMgr, node->getLocStart()));
        Replace::insertAt(fileToReplacements, sourceMgr, location, " FUNCTION_BLOCK;");
        Headers::addHeader(fileToReplacements, sourceMgr, location, Headers::shamanHeader);

        // localisation of the match in human readable format
        std::string readableLoc = location.printToString(*sourceMgr);
        auto functionNode = Result.Nodes.getNodeAs<FunctionDecl>("function");
        std::string functionName = functionNode->getNameAsString();
        DebugLog("Matched function '" + functionName + "' at : " + readableLoc);
    }
};


#endif //SHAMANIZER_FUNCTIONTAGGER_H