#ifndef SHAMANIZER_MATHFUNCTIONS_H
#define SHAMANIZER_MATHFUNCTIONS_H

// clang
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
// shamanizer
#include "../ClangUtilities.h"
#include "../MyMatchers.h"

//---------------------------------------------------------------------------------------
// MATCHERS

/*
 * matches names of mathematical functions
 * TODO update this list
 */
auto MathFunctionNameMatcher =
        hasAnyName("abs", "fabs", "min", "max", "isfinite", "isnan", "isnormal", 
                   "sqrt", "cbrt", "pow", "floor", "ceil", "trunc", "hypot", "fma",
                   "erf", "erff", "erfl", "erfc", "erfcf", "erfcl",
                   "exp", "exp2", "frexp", "ldexp", "log", "log2", "log10",
                   "sin", "cos", "tan", "asin", "acos", "atan", "atan2",
                   "sinh", "cosh", "tanh", "asinh", "acosh", "atanh");

/*
 * matches if all parameters are integers
 */
auto OnlyIntegerParametersMatcher =
        forEachArgumentWithParam(declRefExpr(hasType(isInteger())), parmVarDecl());

/*
 * matches mathematical functions
 * coming from system headers
 * unless all the parameters are integers
 */
auto MathematicFunctionMatcher = [](const std::vector<std::string>& includeFolders)
{
    return callExpr(my_matchers::isInProject(includeFolders),
                    unless(OnlyIntegerParametersMatcher),
                    callee(functionDecl(MathFunctionNameMatcher,
                                        isExpansionInSystemHeader()).bind("function"))
    ).bind("callexpr");
};


//---------------------------------------------------------------------------------------
// HANDLER

/*
 * adds shaman namespace to mathematical functions
 */
class MathFunctionNamespacer : public MatchFinder::MatchCallback
{
private:
    ReplacementsMap& fileToReplacements;

public :
    // constructor
    MathFunctionNamespacer(ReplacementsMap& R) : fileToReplacements(R) {}

    // action that runs on matches
    virtual void run(const MatchFinder::MatchResult &Result)
    {
        // get the node
        auto node = Result.Nodes.getNodeAs<CallExpr>("callexpr");
        auto sourceMgr = Result.SourceManager;
        auto calleeNode = node->getCallee(); // the function call (and not its arguments)
        auto location = Localisation::getMacroLoc(sourceMgr, calleeNode->getLocStart());

        // std::cos => Sstd::cos
        auto functionNode = Result.Nodes.getNodeAs<FunctionDecl>("function"); // the function declaration
        std::string functionName = functionNode->getNameAsString();
        Replace::replaceNode(fileToReplacements, sourceMgr, calleeNode, "Sstd::" + functionName);
        Headers::addHeader(fileToReplacements, sourceMgr, location, Headers::shamanHeader);

        // localisation of the match in human readable format
        std::string readableLoc = location.printToString(*sourceMgr);
        DebugLog("Matched Math function '" + functionName + "' at : " + readableLoc);
    }
};


#endif //SHAMANIZER_MATHFUNCTIONS_H
