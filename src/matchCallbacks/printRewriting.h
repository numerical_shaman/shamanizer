#ifndef SHAMANIZER_PRINTREWRITING_H
#define SHAMANIZER_PRINTREWRITING_H

// std
#include <regex>
#include <sstream>
// clang
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
// shamanizer
#include "../ClangUtilities.h"
#include "changeTypes.h"
#include "../MyMatchers.h"

//---------------------------------------------------------------------------------------
// MATCHERS

/*
 * matches a node with a string literal descendant
 */
auto StringLiteralMatcher =
        hasDescendant(stringLiteral().bind("strLiteral"));

/*
 * matches an argument that contains a string literal
 * (in first or second position if possible)
 */
auto StringLiteralArgMatcher =
        anyOf(hasArgument(0,StringLiteralMatcher),
              hasArgument(1,StringLiteralMatcher),
              hasAnyArgument(StringLiteralMatcher));

/*
 * matches names of printing functions
 * (any function with the printf suffix)
 */
auto PrintFunctionNameMatcher =
        matchesName("[.]*printf$");

/*
 * matches printing functions
 * that has a string literal argument (in first or second position if possible)
 * and at least one floating point argument
 */
auto PrintFunctionMatcher = [](const std::vector<std::string>& includeFolders)
{
    return callExpr(my_matchers::isInProject(includeFolders),
                    StringLiteralArgMatcher,
                    hasAnyArgument(hasType(realFloatingPointType())),
                    callee(functionDecl(PrintFunctionNameMatcher).bind("function"))
    ).bind("callexpr");
};

//---------------------------------------------------------------------------------------
// FUNCTIONS

/*
 * rewrite printf format string to turn %f into %s
 * NOTE : using LLVM regex since regex are not part of the older (before gcc 4.9) stdlib
 */
std::string rewriteFormat(std::string& formatStr, int matchNumber)
{
    // '%' followed by any number of non letter, non '%' chars followed by a floating point compatible format (a, g, e or f)
    llvm::Regex pattern("%[^a-zA-Z%]*[aAgGeEfF]", llvm::Regex::BasicRegex);

    std::string newFormat = formatStr;
    for(int i = 0; i < matchNumber; i++)
    {
        newFormat = pattern.sub("%s", newFormat);
    }

    //std::cerr << "PRINTF FORMAT : replaced '" << formatStr << "' with '" << newFormat << "'." << std::endl;
    return newFormat;
}

/*
 * rewrite a string to prefix every char that needs escaping with a \
 * NOTE could be rewritten with a stringStream for efficiency (but are format string long enough for it to matter ?)
 */
std::string rewriteBackslash(std::string str)
{
    std::string result;

    for(char c : str)
    {
        switch(c)
        {
            case '"':  result += "\\\""; break;
            case '\\': result += "\\\\"; break;
            case '\t': result += "\\t";  break;
            case '\r': result += "\\r";  break;
            case '\n': result += "\\n";  break;
            default:   result += c;      break;
        }
    }

    return result;
}

//---------------------------------------------------------------------------------------
// HANDLER

/*
 * converts printing functions to handle Shaman's types
 * printf("number : %.6f",x) => printf("number : %s",std::to_string(x))
 * NOTE : we could have gone the other way around and matched literal that have a printf father and floats that have a printf father,
 *        this would have avoided searching for particular arguments
 * TODO a solid and better looking solution might be automatic translation of printf into streaming operator (but we lose padding and need to parse formats)
 */
class PrintRewriter : public MatchFinder::MatchCallback
{
private:
    ReplacementsMap& fileToReplacements;

public :
    // constructor
    PrintRewriter(ReplacementsMap& R) : fileToReplacements(R) {}

    // action that runs on matches
    virtual void run(const MatchFinder::MatchResult &Result)
    {
        // get the node
        auto node = Result.Nodes.getNodeAs<CallExpr>("callexpr");
        auto sourceMgr = Result.SourceManager;
        auto location = Localisation::getMacroLoc(sourceMgr, node->getLocStart());

        // wrapping the numerical arguments (and counting their number)
        int matchNumber = 0;
        for(unsigned int arg = 0; arg < node->getNumArgs(); arg++)
        {
            auto argument = node->getArg(arg);
            if(isRealType(argument->getType()))
            {
                Replace::wrapNode(fileToReplacements, sourceMgr, argument, "Sstd::to_Cstring(", ")");
                matchNumber++;
            }
        }

        // replacing the format string
        auto formatNode = Result.Nodes.getNodeAs<clang::StringLiteral>("strLiteral");
        std::string format = formatNode->getString();
        std::string newFormat = rewriteBackslash(rewriteFormat(format, matchNumber));
        Replace::replaceNode(fileToReplacements, sourceMgr, formatNode, "\"" + newFormat + "\"");
        Headers::addHeader(fileToReplacements, sourceMgr, location, Headers::shamanHeader);

        // extracting the function
        auto functionNode = Result.Nodes.getNodeAs<FunctionDecl>("function"); // the function declaration
        std::string functionName = functionNode->getNameAsString();

        // localisation of the match in human readable format
        std::string readableLoc = location.printToString(*sourceMgr);
        DebugLog("Matched Print function '" + functionName + "' at : " + readableLoc);
    }
};

#endif //SHAMANIZER_PRINTREWRITING_H