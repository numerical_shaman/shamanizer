#ifndef SHAMANIZER_CHANGETYPES_H
#define SHAMANIZER_CHANGETYPES_H

// clang
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
// shamanizer
#include "../ClangUtilities.h"
#include "../MyMatchers.h"

//---------------------------------------------------------------------------------------
// MATCHERS

/*
 * matches floating point types
 * similar to strictRealFloatingPointType but do not match type alias
 */
ast_matchers::internal::Matcher<QualType> strictRealFloatingPointType =
        anyOf(asString("float"), asString("double"), asString("long double"));

/*
 * match double in types
 * that are in the project files
 * but are not the result of a substitution of a template parameter
 */
auto DoubleTypeMatcher = [](const std::vector<std::string>& includeFolders)
{
    return typeLoc(my_matchers::isInProject(includeFolders),
                   loc(strictRealFloatingPointType),
                   unless(loc(substTemplateTypeParmType()))
    ).bind("type");
};

//---------------------------------------------------------------------------------------
// FUNCTIONS

/*
 * takes a type and returns an Stype
 * NOTE : working directly on QualType might improve performances
 */
std::string StypeOfType(std::string type)
{
    if (type == "float") return "Sfloat";
    if (type == "double") return "Sdouble";
    if (type == "long double") return "Slong_double";
    throw std::invalid_argument("StypeOfType : Received a type ('" + type + "') that is not a known floating point type.");
}

/*
 * returns true if the ubnderlying type is a floatting point type
 */
bool isRealType(QualType t)
{
    std::string typeStr = t.getCanonicalType().getAsString();
    return (typeStr == "double") || (typeStr == "float") || (typeStr == "long double");
};

//---------------------------------------------------------------------------------------
// HANDLER

/*
 * replace double types with Sdouble
 */
class TypeSwitcher : public MatchFinder::MatchCallback
{
private:
    ReplacementsMap& fileToReplacements;

public :
    // constructor
    TypeSwitcher(ReplacementsMap& R) : fileToReplacements(R) {}

    // action that runs on matches
    virtual void run(const MatchFinder::MatchResult &Result)
    {
        // get the needed data
        auto node = Result.Nodes.getNodeAs<TypeLoc>("type");
        auto sourceMgr = Result.SourceManager;
        auto location = Localisation::getMacroLoc(sourceMgr, node->getLocStart());

        // replaces the type : double* => Sdouble*
        std::string type = node->getType().getAsString();
        Replace::replaceNode(fileToReplacements, sourceMgr, node, StypeOfType(type));
        Headers::addHeader(fileToReplacements, sourceMgr, location, Headers::shamanHeader);

        // displays the match in human readable format
        std::string readableLoc = location.printToString(*sourceMgr);
        DebugLog("Matched " + type + " at : " + readableLoc);
    }
};

#endif //SHAMANIZER_CHANGETYPES_H