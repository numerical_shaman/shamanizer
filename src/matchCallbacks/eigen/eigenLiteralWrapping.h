#ifndef SHAMANIZER_EIGENLITERALWRAPPING_H
#define SHAMANIZER_EIGENLITERALWRAPPING_H

// clang
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
// shamanizer
#include "../../ClangUtilities.h"
#include "../../MyMatchers.h"

//---------------------------------------------------------------------------------------
// MATCHERS

/*
 * matches calls to eigen functions
 */
auto EigenFunctionMatcher =
        callExpr(callee(functionDecl(matchesName("Eigen::.*")).bind("functionDecl")) );

/*
 * match double literal arguments in Eigen's functions
 */
auto EigenLiteralMatcher = [](const std::vector<std::string>& includeFolders)
{
    return floatLiteral(my_matchers::isInProject(includeFolders),
                        hasAncestor(EigenFunctionMatcher)
    ).bind("literal");
};

/*
 * matches calls to one of eigen's operator
 * use cxxOperatorCallExpr to match overload (because we are looking for overload and not simple binary operators)
 */
auto EigenOperatorMatcher =
        cxxOperatorCallExpr(callExpr(callee(functionDecl(matchesName("Eigen::.*operator")))));

/*
 * an expression
 * in the project
 * that has an integer type (including const and other derived types)
 * that is the children of an Eigen operator (and thus will be casted)
 */
auto EigenIntLiteralMatcher = [](const std::vector<std::string>& includeFolders)
{
    return expr(my_matchers::isInProject(includeFolders),
                hasType(isInteger()),
                hasParent(EigenOperatorMatcher)
    ).bind("literal");
};

//---------------------------------------------------------------------------------------
// HANDLER

/*
 * wraps int literal in Sdouble
 */
class IntLiteralWrapper : public MatchFinder::MatchCallback
{
private:
    ReplacementsMap& fileToReplacements;

public :
    // constructor
    IntLiteralWrapper(ReplacementsMap& R) : fileToReplacements(R) {}

    // action that runs on matches
    virtual void run(const MatchFinder::MatchResult &Result)
    {
        // get the node
        auto node = Result.Nodes.getNodeAs<Expr>("literal");
        auto sourceMgr = Result.SourceManager;
        auto location = Localisation::getMacroLoc(sourceMgr, node->getLocStart());
        if(location.isInvalid()) return; // security: exit if the location is invalid

        // wraps the int litteral : 25 -> Sdouble(25)
        std::string type = node->getType().getAsString();
        Replace::wrapNode(fileToReplacements, sourceMgr, node, "Sdouble(", ")");
        Headers::addHeader(fileToReplacements, sourceMgr, location, Headers::shamanHeader);

        // localisation of the match in human readable format
        std::string readableLoc = location.printToString(*sourceMgr);
        DebugLog("Matched " + type + " literal at : " + readableLoc);
    }
};

#endif //SHAMANIZER_EIGENLITERALWRAPPING_H