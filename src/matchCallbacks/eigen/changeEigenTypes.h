#ifndef SHAMANIZER_CHANGEEIGENTYPES_H
#define SHAMANIZER_CHANGEEIGENTYPES_H

// clang
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
// shamanizer
#include "../../ClangUtilities.h"
#include "../../MyMatchers.h"

//---------------------------------------------------------------------------------------
// MATCHERS

/*
 * matches Eigen typedefs starting with Matrix or Vector and ending with d of f
 */
ast_matchers::internal::Matcher<QualType> EigenType =
        anyOf(my_matchers::matchesString("^Eigen::Matrix.*[df]$"), my_matchers::matchesString("^Eigen::Vector.*[df]$"));

/*
 * match eigen types in types
 * that are in the project files
 * but are not the result of a substitution of a template parameter
 */
auto EigenTypeMatcher = [](const std::vector<std::string>& includeFolders)
{
    return typeLoc(my_matchers::isInProject(includeFolders),
                   loc(EigenType),
                   unless(loc(substTemplateTypeParmType()))
    ).bind("type");
};

//---------------------------------------------------------------------------------------
// FUNCTION

/*
 * takes an eigen type and returns an Stype
 * Eigen::MatrixXd => Eigen::SMatrixXd"
 */
std::string StypeOfEigenType(std::string type)
{
    type = String::replace("Eigen::Vector", "Eigen::SVector", type);
    type = String::replace("Eigen::Matrix", "Eigen::SMatrix", type);
    return type;
}

//---------------------------------------------------------------------------------------
// HANDLER

/*
 * replace Eigen types with shaman_Eigen types
 * TODO we should also wrap numbers interacting with eigen types (as argument of function or in binary operations)
 */
class EigenTypeSwitcher : public MatchFinder::MatchCallback
{
private:
    ReplacementsMap& fileToReplacements;

public :
    // constructor
    EigenTypeSwitcher(ReplacementsMap& R) : fileToReplacements(R) {}

    // action that runs on matches
    virtual void run(const MatchFinder::MatchResult &Result)
    {
        // get the needed data
        auto node = Result.Nodes.getNodeAs<TypeLoc>("type");
        auto sourceMgr = Result.SourceManager;
        auto location = Localisation::getMacroLoc(sourceMgr, node->getLocStart());

        // replaces the type : Matrix2cd* => SMatrix2cd*
        std::string type = node->getType().getAsString();
        Replace::replaceNode(fileToReplacements, sourceMgr, node, StypeOfEigenType(type));
        Headers::addHeader(fileToReplacements, sourceMgr, location, Headers::shamanEigenHeader);
        Headers::addHeader(fileToReplacements, sourceMgr, location, Headers::shamanHeader); // put after eigen header to be included first

        // displays the match in human readable format
        std::string readableLoc = location.printToString(*sourceMgr);
        DebugLog("Matched " + type + " (Eigen type) at : " + readableLoc);
    }
};

#endif //SHAMANIZER_CHANGEEIGENTYPES_H