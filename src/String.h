#ifndef SHAMANIZER_STRING_H
#define SHAMANIZER_STRING_H

// std
#include <algorithm>
#include <string>

namespace String
{
    // trim string from end (in place)
    // cf https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
    void trimEnd(std::string& s)
    {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(), s.end());
    }

    // returns true if the string starts with the given prefix
    bool startsWith(const std::string& str, const std::string& prefix)
    {
        return str.rfind(prefix, 0) == 0;
    }

    // returns true if the string starts with one of the given prefix
    bool startsWith(const std::string& str, const std::vector<std::string>& prefixes)
    {
        for(const auto& prefix : prefixes)
        {
            if(startsWith(str,prefix))
            {
                return true;
            }
        }
        return false;
    }

    // replace all occurences of a substring
    std::string replace(std::string substring, std::string newSubString, std::string str)
    {
        size_t index = str.find(substring, 0);

        while (index != std::string::npos)
        {
            // Make the replacement.
            str.replace(index, substring.size(), newSubString);

            // Advance index forward so the next iteration doesn't pick it up as well.
            index += newSubString.size();

            // Locate the new substring to replace.
            index = str.find(substring, index);
        }

        return str;
    }
}

#endif //SHAMANIZER_STRING_H