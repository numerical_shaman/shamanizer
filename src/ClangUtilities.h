#ifndef SHAMANIZER_REPLACEMENTS_H
#define SHAMANIZER_REPLACEMENTS_H

// std
#include <unordered_map>
#include <unordered_set>
// clang
#include "clang/Lex/Lexer.h"
#include "clang/Tooling/Refactoring.h"
// shamanizer
#include "CmdArguments.h"

using namespace clang;

// stores a replacement map and a list of headers per file
class ReplacementsMap
{
public:
    // table of filepath->replacements to store each replacements
    std::map<std::string, tooling::Replacements>* map;
    // table of filepath->set(header) to check wether an header has alreaddy been added to a file
    std::unordered_map<std::string, std::unordered_set<std::string>> headersPerFile;
    // vector of include path (that should not be touched)
    std::vector<std::string>& includeDirectories;

    // constructors that store a pointer to a replacementMap
    ReplacementsMap(std::map<std::string, tooling::Replacements>* map, std::vector<std::string>& includeDirectories): map(map), includeDirectories(includeDirectories), headersPerFile() {};

    // adds a replacement to the map
    // unless the file is in one of the include directories
    // returns true in case of success
    // if merge is allowed, conflict will be resolved by a merge (which could led to incorrect outputs in complex case)
    void add(const std::string& filename, tooling::Replacement& replacement, bool allowMerge = false)
    {
        // action called in case of conflict
        auto errorHandler = [this,filename,replacement,allowMerge](const llvm::ErrorInfoBase& E)
        {
            if (allowMerge)
            {
                tooling::Replacements replacements(replacement);
                (*map)[filename] = (*map)[filename].merge(replacements);
                //DebugLog("Performed an order dependant merge in file : " + filename);
            }
            else
            {
                DebugLog("Refused instrumentation due to conflict : " + replacement.toString());
            }
        };

        // performs the add
        llvm::handleAllErrors((*map)[filename].add(replacement), errorHandler);
    };
};

// utilitary functions for localisation
namespace Localisation
{
    // gets spelling location if we are in a macro
    SourceLocation getMacroLoc(const SourceManager* sourceMgr, SourceLocation location)
    {
        if (location.isMacroID())
        {
            return sourceMgr->getSpellingLoc(location);
        }
        else
        {
            return location;
        }
    }

    // gets location after the token
    SourceLocation getLocAfterToken(const SourceManager* sourceMgr, SourceLocation location)
    {
        return Lexer::getLocForEndOfToken(location, 0, *sourceMgr, LangOptions());
    }
}

// utilitary functions for replacements
namespace Replace
{
    // inserts the text at the given location
    void insertAt(ReplacementsMap& fileToReplacements, const SourceManager* sourceMgr, SourceLocation location, const std::string& text, bool allowMerge = false)
    {
        tooling::Replacement replacement(*sourceMgr, location, 0, text);
        std::string filename = sourceMgr->getFilename(location);
        fileToReplacements.add(filename, replacement, allowMerge);
    }

    // replace the node with the given text
    template<typename Node>
    void replaceNode(ReplacementsMap& fileToReplacements, const SourceManager* sourceMgr, const Node* node, const std::string& text)
    {
        auto location = Localisation::getMacroLoc(sourceMgr, node->getLocStart());
        tooling::Replacement replacement(*sourceMgr, node, text);
        std::string filename = sourceMgr->getFilename(location);
        fileToReplacements.add(filename, replacement);
    }

    // wraps the node with the given text
    template<typename Node>
    void wrapNode(ReplacementsMap& fileToReplacements, const SourceManager* sourceMgr, const Node* node, const std::string& textBefore, const std::string& textAfter)
    {
        auto location1 = Localisation::getMacroLoc(sourceMgr, node->getLocStart());
        auto location2 = Localisation::getLocAfterToken(sourceMgr, Localisation::getMacroLoc(sourceMgr, node->getLocEnd()));
        std::string filename = sourceMgr->getFilename(location1);

        tooling::Replacement replacement1(*sourceMgr, location1, 0, textBefore);
        tooling::Replacement replacement2(*sourceMgr, location2, 0, textAfter);

        fileToReplacements.add(filename, replacement1);
        fileToReplacements.add(filename, replacement2);
    }
}

// header manipulation
namespace Headers
{
    const std::string shamanHeader = "#include <shaman.h>";
    const std::string shamanEigenHeader = "#include <shaman/helpers/shaman_eigen.h>";

    // adds an header at the begining of the current file
    void addHeader(ReplacementsMap& fileToReplacements, const SourceManager* sourceMgr, SourceLocation location, const std::string& header)
    {
        const std::string& filename = sourceMgr->getFilename(location);

        // gets headerSet (iomplicitely creates it if it does not exists)
        std::unordered_set<std::string>& headerSet = fileToReplacements.headersPerFile[filename];

        // adds header to the headerSet and file if needed
        if(headerSet.find(header) == headerSet.end())
        {
            auto fileId = sourceMgr->getFileID(location);
            auto startingLocation = sourceMgr->getLocForStartOfFile(fileId);
            headerSet.insert(header);
            Replace::insertAt(fileToReplacements, sourceMgr, startingLocation, header + "\n", true/*allow merge*/);
            DebugLog("Header '" + header + "' added to : " + filename);
        }
    }
}

#endif //SHAMANIZER_REPLACEMENTS_H
