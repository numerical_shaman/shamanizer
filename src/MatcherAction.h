#ifndef SHAMANIZER_MATCHERACTION_H
#define SHAMANIZER_MATCHERACTION_H

// clang AST matching
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang;
using namespace clang::ast_matchers;

// matchers
#include "ClangUtilities.h"
#include "MyMatchers.h"
#include "matchCallbacks/changeTypes.h"
#include "matchCallbacks/mathFunctions.h"
#include "matchCallbacks/floatLiteralWrapping.h"
#include "matchCallbacks/functionTagger.h"
#include "matchCallbacks/printRewriting.h"
#include "matchCallbacks/eigen/changeEigenTypes.h"
#include "matchCallbacks/eigen/eigenLiteralWrapping.h"
#include "matchCallbacks/mallocWarning.h"

//-----------------------------------------------------------------------------
// AST CONSUMER

/*
 * Implementation of the ASTConsumer interface for reading an AST produced by the Clang parser.
 * It registers a couple of matchers and runs them on the AST :
 * - changes types
 * - wraps literals in type casts
 * - adds tag to function body (TODO could be prenumbered cleverly to improve perfs)
 * - adds include when needed (TODO could be placed more cleverly)
 * - changes mathematical functions
 * - throw warnings on malloc/free (but do not try to rewrite them)
 * - adapts printing statement
 * TODO Eigen
 * TODO MPI (useless if we have no working prototype of shaman with both tagged error and MPI)
 */
class MyASTConsumer : public ASTConsumer
{
private:
    MatchFinder Matcher;
    ReplacementsMap& replacementsMap;
    // handlers
    TypeSwitcher TypeSwitchHandler;
    LiteralWrapper LiteralWrapperHandler;
    FunctionBodyTagger FunctionBodyTagHandler;
    MathFunctionNamespacer MathFunctionNamespaceHandler;
    PrintRewriter PrintRewritingHandler;
    EigenTypeSwitcher EigenTypeSwitchHandler;
    IntLiteralWrapper IntLiteralWrapperHandler;
    MallocWarner MallocWarningHandler;

public:
    // binds patterns and handlers
    MyASTConsumer(ReplacementsMap& R): TypeSwitchHandler(R), LiteralWrapperHandler(R),
                                       FunctionBodyTagHandler(R), MathFunctionNamespaceHandler(R),
                                       MallocWarningHandler(R), PrintRewritingHandler(R),
                                       EigenTypeSwitchHandler(R), replacementsMap(R),
                                       IntLiteralWrapperHandler(R)
    {
        const auto& includeFolders = replacementsMap.includeDirectories;

        // shaman
        Matcher.addMatcher(DoubleTypeMatcher(includeFolders), &TypeSwitchHandler); // type substitution
        Matcher.addMatcher(FunctionBodyMatcher(includeFolders), &FunctionBodyTagHandler); // adding tags to function bodies
        Matcher.addMatcher(MathematicFunctionMatcher(includeFolders), &MathFunctionNamespaceHandler); // adding namespace to math functions
        Matcher.addMatcher(PrintFunctionMatcher(includeFolders), &PrintRewritingHandler); // rewrites printf functions

        // eigen
        ast_matchers::internal::Matcher<Stmt> generalLiteralMatcher = anyOf(DoubleLiteralMatcher(includeFolders), EigenLiteralMatcher(includeFolders));
        Matcher.addMatcher(EigenTypeMatcher(includeFolders), &EigenTypeSwitchHandler); // eigen type substitution
        Matcher.addMatcher(generalLiteralMatcher, &LiteralWrapperHandler); // literal wrapping
        Matcher.addMatcher(EigenIntLiteralMatcher(includeFolders), &IntLiteralWrapperHandler); // literal wrapping

        // warnings
        Matcher.addMatcher(MemoryFunctionMatcher(includeFolders), &MallocWarningHandler); // raises warning when a malloc is located
    }

    // Run the matchers when we have the whole TU parsed.
    void HandleTranslationUnit(ASTContext& Context) override
    {
        Matcher.matchAST(Context);
    }
};

//-----------------------------------------------------------------------------
// FRONTEND ACTION

/*
 * For each source file provided to the tool, a new FrontendAction is created.
 * needs to implements newASTConsumer (for newFrontendActionFactory)
 */
struct MyFrontendAction
{
private:
    ReplacementsMap replacementsMap;

public:
    // constructor
    MyFrontendAction(ReplacementsMap& replacementsMap): replacementsMap(replacementsMap) {};

    // produces an AST consumer
    std::unique_ptr<ASTConsumer> newASTConsumer()
    {
        return llvm::make_unique<MyASTConsumer>(replacementsMap);
    };
};

#endif //SHAMANIZER_MATCHERACTION_H