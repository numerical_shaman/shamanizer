#ifndef SHAMANIZER_MATCHERS_H
#define SHAMANIZER_MATCHERS_H

// clang
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
using namespace clang::ast_matchers;
using namespace clang;
// shamanizer
#include "String.h"

/*
 * contains personal matchers
 * see preexisting matchers at : http://clang.llvm.org/docs/LibASTMatchersReference.html
 * see implementation at : https://clang.llvm.org/doxygen/ASTMatchers_8h_source.html
 * using macros from : https://clang.llvm.org/doxygen/ASTMatchersMacros_8h_source.html#l00264
 */
namespace my_matchers
{
    /*
     * test wether a name starts with the given string
     * Matcher<Qualtype>    startsWithString   std::string
     */
    AST_MATCHER_P(QualType, startsWithString, std::string, prefix)
    {
        return String::startsWith(Node.getAsString(), prefix);
    }

    /*
    * test wether a name matches with the given regexp
    * Matcher<Qualtype>    matchesString   std::string
    */
    AST_MATCHER_P(QualType, matchesString, std::string, RegExp)
    {
        assert(!RegExp.empty());
        std::string filename = Node.getAsString();
        llvm::Regex RE(RegExp);
        return RE.match(filename);
    }

    /*
     * matches if the node is in one of the given folders
     * Matcher<Decl / Stmt / TypeLoc>    isExpansionInFolders    std::vector<std::string>
     */
    AST_POLYMORPHIC_MATCHER_P(isExpansionInFolders, AST_POLYMORPHIC_SUPPORTED_TYPES(Decl, Stmt, TypeLoc), std::vector<std::string>, folders)
    {
        auto &SourceManager = Finder->getASTContext().getSourceManager();

        auto ExpansionLoc = SourceManager.getExpansionLoc(Node.getLocStart());
        if (ExpansionLoc.isInvalid()) return false;

        auto FileEntry = SourceManager.getFileEntryForID(SourceManager.getFileID(ExpansionLoc));
        if (!FileEntry) return false;

        auto Filename = FileEntry->getName();
        return String::startsWith(Filename,folders);
    }

    /*
     * matches files that belong to the current project
     * isExpansionInMainFile matches only files in compilation database
     * isExpansionInSystemHeader match most system headers
     * isExpansionInFolders match any given header
     */
    auto isInProject = [](const std::vector<std::string>& includeFolders)
    {
        return unless(anyOf(isExpansionInSystemHeader(),
                            my_matchers::isExpansionInFolders(includeFolders)));
    };
}

#endif //SHAMANIZER_MATCHERS_H