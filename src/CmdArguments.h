#ifndef SHAMANIZER_SHELLCMD_H
#define SHAMANIZER_SHELLCMD_H

// std
#include <memory>
#include <string>
// clang
#include "clang/Tooling/CommonOptionsParser.h"
// shamanizer
#include "parameters.h"
#include "String.h"

// runs a command in shell and retruns the result
// cf https://stackoverflow.com/a/478960/6422174
// NOTE specific to POSIX systems
std::string executeCmd(const char* cmd)
{
    // run command
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");

    // stores output in result
    std::string result;
    std::array<char, 128> buffer;
    while (!feof(pipe.get()))
    {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
        {
            result += buffer.data();
        }
    }

    // trim trailing whitespaces
    String::trimEnd(result);
    return result;
}

// outputs debug logs
void DebugLog(const std::string message)
{
    if(DisplayLogs)
    {
        llvm::outs() << message << '\n';
    }
}

// extract paths to included folders (lines starting with -I)
std::vector<std::string> extractIncludeFolders(clang::tooling::CompilationDatabase& compilationDatabase)
{
    std::vector<std::string> includeDirectories;

    for(auto& cmd : compilationDatabase.getAllCompileCommands())
    {
        for(auto& line : cmd.CommandLine)
        {
            if(String::startsWith(line,"-I"))
            {
                if(line == "-I")
                {
                    std::string errorMessage = "The compilation database contains several include for a single '-I', we are unable to exclude them from the refactoring.";
                    throw std::invalid_argument(errorMessage);
                }
                else
                {
                    std::string folderPath = line.substr(2);
                    includeDirectories.push_back(folderPath);
                    DebugLog("Located include directory at " + folderPath);
                }
            }
        }
    }

    return includeDirectories;
}

#endif //SHAMANIZER_SHELLCMD_H