# Shamanizer

A tool that instruments a code using Clang (LibTooling) to use [Shaman](https://gitlab.com/numerical_shaman/shaman) types instead of classical numerical types.

## Usage :

### Running the tool :

First, insure that your project compiles with Clang before trying to instrument it.
The instrumentation *will be cancelled* (leaving the files untouched) if there is any compilation error.

For very simple, single file, applications, you can just run `shamanizer my_file.cpp --`.

For larger / more complex projects, you will need a compilation database. You can build one with [Clang](https://sarcasm.github.io/notes/dev/compilation-database.html#clang) or [CMake](https://sarcasm.github.io/notes/dev/compilation-database.html#cmake).
Once you have a compilation database built for your project, you can run `shamanizer -p=my_compilation_database.json my_file.cpp`.
Use `.` instead of a filename to run on all files in the compilation database.

Note that clang include files will be needed if Shamanizer is not in `usr/bin`, they can be located using `$(dirname $(which clang))/../lib/clang/7.0.0/include`.
You can then start `shamanizer -p=my_compilation_database.json . -extra-arg=-isystem=/path/to/include`.

### Instalation with Spack :

We recommend that you use [Spack_Shaman](https://gitlab.com/numerical_shaman/spack_shaman) in order to install Shamanizer with Spack without having to worry about its dependencies.

### Installing LLVM to build the tool from source :

Official Ubuntu/Debian packages currently have broken cmakefiles, the solution is to get the official LLVM packages from [apt.llvm.org](http://apt.llvm.org/) following the protocal given [here](https://askubuntu.com/questions/787383/how-to-install-llvm-3-9).

Once you have installed all the packages from the stable branch (only half of them are truly needed), you should be good to go (previously installed LLVM versions could cause problems, in which case the easier solution is to uninstall them).

If you cannot use a package manager you will need to recompile LLVM and Clang from source (note : if your RAM is insuficient for the linking step, setting the number of parallel workers to 1 (using `-j1`) should do the trick).

## Contributing

See `contributing.md` for some documentation useful to get started with the code base and a list of improvements that we would like to see implemented.

